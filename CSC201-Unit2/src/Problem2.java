import java.util.Scanner;


public class Problem2 {
	//Algorithm to solve the problem
	//1) create a method to get the individual scores 
	//2) create a method to create an array with the previous method return statement
	//3) create a method to find out the degree of difficulty
	//4) create a method to find the index of the high and low
	//5) add the sum of the high and low
	//6) use the scores and degree of difficulty to find the final result
	
	public static void main(String[] args) {

		calculateScore(); 
	}

	private static float inputValidScore() {
		//allows the user to enter a score between 0 and 10
		//not allowed to enter anything else
		
		System.out.println("Enter the score for the diver between 0 and 10");
		Scanner keyboard = new Scanner(System.in);
		float score = keyboard.nextInt();
		while(score>10 || score <0)
		{
			System.out.println("You have entered an invalid score, please enter a number between 0 and 10.");
			score = keyboard.nextInt();
		}
		
		return score; //the score rating will be returned and used in a different method
	}
	private static float[] inputAllScores() 
	{
		//uses the previous score to enter them into this array
		//the array is of size 7
		
		float[] diverScore = new float[7];
		int i = 0;
		while(i<7)
		{
			float size = inputValidScore();	
			diverScore[i] =	(int) size; 
			i++;
		}	
		return diverScore;
	}
	private static float inputValidDegreeOfDifficulty() {
		//allows the user to enter the difficulty
		//will be returned and used in a different method
		
		System.out.println("Please enter a valid degree of difficulty between 1.2 and 3.8.");
		Scanner keyboard = new Scanner(System.in);
		float difficulty = (float) keyboard.nextDouble();
		while(difficulty>3.8 || difficulty <1.2)
		{
			System.out.println("You have entered an invalid score, please enter a number between 1.2 and 3.8.");
			difficulty = (float) keyboard.nextDouble();
		}
		
		return difficulty;

	}
	private static float calculateScore() {
		//finds out the index of high and low
		
		float low =11;
		float high =0;
		float sum =0;
		int indexLow = 0, indexHigh = 0;
		float[] scoreOfDiver = inputAllScores();
		//		scoreOfDiver[0] = high;
		//		scoreOfDiver[0] = low;

		for(int i =0 ; i<7; i++ )
		{
			if(scoreOfDiver[i] < low)
			{
				low = scoreOfDiver[i];
				indexLow = i;

			}
			else if(scoreOfDiver[i] > high)
			{
				high = scoreOfDiver[i];
				indexHigh = i; 
			}

		}
		for(int i =0; i<7; i++)
		{
			if((!(scoreOfDiver[i]==scoreOfDiver[indexLow] || scoreOfDiver[i]==scoreOfDiver[indexHigh])))
			{
				sum = sum + scoreOfDiver[i];
				
			}
		}
		//use the scores to calculate the final score
		float totalScore;
		double difficultyScore = inputValidDegreeOfDifficulty();
		totalScore = (float) (sum * difficultyScore); 
		totalScore = (float) (totalScore * .6);
		System.out.println("The total score the diver received was: " + totalScore);
		

		return totalScore;

	}
}
