import java.util.Scanner;


public class Problem2 {
	//Algorithm to solve the problem
	//1) create a method to get the individual scores 
	//2) create a method to create an array with the previous method return statement
	//3) create a method to find out the degree of difficulty
	//4) create a method to find the index of the high and low
	//5) add the sum of the high and low
	//6) use the scores and degree of difficulty to find the final result
	
	public static void main(String[] args) {

		calculateScore(); 
	}

	private static int inputValidScore() {
		// TODO Auto-generated method stub
		//allows the user to enter a score between 0 and 10
		//not allowed to enter anything else
		
		System.out.println("Enter the score for the diver between 0 and 10");
		Scanner keyboard = new Scanner(System.in);
		int score = keyboard.nextInt();
		while(score>10 || score <0)
		{
			System.out.println("You have entered an invalid score, please enter a number between 0 and 10.");
			score = keyboard.nextInt();
		}
		
		return score; //the score rating will be returned and used in a different method
	}
	private static int[] inputAllScores() 
	{
		//uses the previous score to enter them into this array
		//the array is of size 7
		
		int[] diverScore = new int[7];
		int i = 0;
		while(i<7)
		{
			int size = inputValidScore();	
			diverScore[i] =	size; 
			i++;
		}	
		return diverScore;
	}
	private static double inputValidDegreeOfDifficulty() {
		//allows the user to enter the difficulty
		//will be returned and used in a different method
		
		System.out.println("Please enter a valid degree of difficulty.");
		Scanner keyboard = new Scanner(System.in);
		double difficulty = keyboard.nextDouble();
		return difficulty;

	}
	private static double calculateScore() {
		//finds out the index of high and low
		
		int low =11, high =0, sum =0, indexLow = 0, indexHigh = 0;
		int[] scoreOfDiver = inputAllScores();
		//		scoreOfDiver[0] = high;
		//		scoreOfDiver[0] = low;

		for(int i =0 ; i<7; i++ )
		{
			if(scoreOfDiver[i] < low)
			{
				low = scoreOfDiver[i];
				indexLow = i;

			}
			else if(scoreOfDiver[i] > high)
			{
				high = scoreOfDiver[i];
				indexHigh = i; 
			}

		}
		for(int i =0; i<7; i++)
		{
			if((!(scoreOfDiver[i]==scoreOfDiver[indexLow] || scoreOfDiver[i]==scoreOfDiver[indexHigh])))
			{
				sum = sum + scoreOfDiver[i];
				
			}
		}
		//use the scores to calculate the final score
		double totalScore;
		double difficultyScore = inputValidDegreeOfDifficulty();
		totalScore = sum * difficultyScore; 
		totalScore = totalScore * .6;
		System.out.println("The total score the diver received was: " + totalScore);
		

		return totalScore;

	}
}
